<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * Referred to as testBasicTest in the Cloudways article, because
     * that refers to an older version of Laravel.
     *
     * @return void
     */
    public function test_the_application_returns_a_successful_response()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testIndexView()
    {
        $view = $this->view('index');
        $view->assertSee('Homepage');
    }

    public function testAboutView()
    {
        $view = $this->view('about');
        $view->assertSee('About');
    }
}
