@extends('layouts.master')

@section('title')
    <title>GitLab Cloudways Integration Tutorial</title>
@endsection

@section('headline')
    <h1 class="headline">Homepage</h1>
@endsection

@section('content')
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed atque officiis in odio. Consequuntur dolorum laborum
        dolores nemo, voluptatem possimus voluptas libero autem rem! Expedita quam hic consequuntur accusamus labore.</p>
    <p>Hello, World!</p>
    <a href="/about">Go to About</a>
@endsection
