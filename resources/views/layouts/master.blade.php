<html>

<head>
    @yield('title')
    <link rel="icon" href="favicon.ico" />
</head>

<body>
    @yield('headline')

    @section('sidebar')
        This is the master sidebar.
    @show

    <div class="container">
        @yield('content')
    </div>
</body>

</html>
