@extends('layouts.master')

@section('title')
    <title>GitLab Cloudways Integration Tutorial</title>
@endsection

@section('headline')
    <h1 class="headline">About</h1>
@endsection

@section('content')
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed atque officiis in odio. Consequuntur dolorum laborum
        dolores nemo, voluptatem possimus voluptas libero autem rem! Expedita quam hic consequuntur accusamus labore.</p>
    <a href="/">Go back Home</a>
@endsection
